angular.module('dashboard', [])
.controller('PrintController', function($scope, $http) {
  $scope.load = function() {
    loadFromJira($scope.username, $scope.password, $scope.jql)
  }
  $scope.print = function() {
    window.print();
  }
  function loadFromJira(user, pass, jql) {
    $scope.loading = true;
    $http({
      method: 'GET',
      url: 'https://jira.heg.com/rest/api/2/search?jql='+jql,
      headers: {
        Authorization: 'Basic ' + btoa(user + ':' + pass),
      }
    }).then(function (response) {
      console.log(response);
      $scope.issues = response.data.issues;
      $scope.loading = false;
    }, function (response) {
      console.log(response);
      $scope.loading = false;
    });
  }
});
